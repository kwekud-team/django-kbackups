from django.views.generic import View
from kommons.utils.http import redirect_to_referer
from kbackups.utils import BackupUtils, StorageUtils
from kbackups.models import BackupSetup, BackupLog


class LoadConfigView(View):

    def get(self, request, *args, **kwargs):
        BackupUtils(kwargs['type']).load_configs_from_settings()

        return redirect_to_referer(request)


class RunDatabaseView(View):

    def get(self, request, *args, **kwargs):
        pk_list = request.GET.get('pks', '').split(',')

        for pk in pk_list:
            backup_setup = BackupSetup.objects.filter(pk=pk).first()
            if backup_setup:
                BackupUtils(backup_setup.type).backup_database(backup_setup)

        return redirect_to_referer(request)


class SyncFileView(View):

    def get(self, request, *args, **kwargs):
        pk_list = request.GET.get('pks', '').split(',')

        for pk in pk_list:
            backup_log = BackupLog.objects.filter(pk=pk).first()
            if backup_log:
                StorageUtils().push_file_remote(backup_log)

        return redirect_to_referer(request)