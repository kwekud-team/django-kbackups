from enum import Enum


class KbackupsK(Enum):
    TYPE_DATABASE = 'type_database'
    TYPE_MEDIA = 'type_media'

    SYNC_TYPE_SCP = 'sync_type_scp'
    SYNC_TYPE_DROPBOX = 'sync_type_dropbox'
