import os
import hashlib
from django.db import connection
from django.conf import settings
from django.core import management
from django.utils import timezone
from dbbackup.management.commands import dbbackup, mediabackup
from kommons.utils.files import mkdir_p, is_file_exists
from kbackups.models import BackupSetup, BackupLog, SyncSetup, SyncLog
from kbackups.constants import KbackupsK


class BackendBase:

    def gen_unique_key(self):
        txt = '|'.join(['%s:%s' % (k,v) for k,v in self.get_extra().items()])
        return hashlib.md5(txt.encode('utf-8')).hexdigest()

    def get_extra(self):
        return {}

    def do_backup(self, backup_setup, full_path):
        pass

    def get_backup_filename(self, backup_setup):
        return '_tmp'


class BackupDatabaseBackend(BackendBase):

    def get_backup_filename(self, backup_setup):
        now = timezone.now()
        return 'db_%s_%s.sql' % (backup_setup.extra['database'], now.strftime('%Y%m%d%H%M'))

    def get_extra(self):
        values = connection.settings_dict
        return {
            'using': 'default',
            'engine': values['ENGINE'],
            'database': values['NAME'],
            'username': values['USER'],
            'host': values['HOST'],
            'port': values['PORT']
        }

    def do_backup(self, backup_setup, full_path):
        management.call_command(dbbackup.Command(), database=backup_setup.extra['using'], output_path=full_path)


class BackupMediaBackend(BackendBase):

    def get_backup_filename(self, backup_setup):
        now = timezone.now()
        return 'media_%s_%s.tar' % (backup_setup.extra['database'], now.strftime('%Y%m%d%H%M'))

    def get_extra(self):
        values = connection.settings_dict
        return {
            'database': values['NAME'],
            'media_url': settings.MEDIA_URL,
        }

    def do_backup(self, backup_setup, full_path):
        management.call_command(mediabackup.Command(), output_path=full_path, compress=True)


class BackupUtils:

    def __init__(self, ttype):
        self.type = ttype
        self.backend = self.get_backend()
        self.media_root = settings.MEDIA_ROOT
        self.output_folder = 'store/kbackups/backup_logs/'

    def get_backend(self):
        backends = {
            KbackupsK.TYPE_DATABASE.value: BackupDatabaseBackend,
            KbackupsK.TYPE_MEDIA.value: BackupMediaBackend,
        }
        return backends.get(self.type, BackendBase)()

    def load_configs_from_settings(self):
        BackupSetup.objects.update_or_create(
            unique_key=self.backend.gen_unique_key(),
            type=self.type,
            defaults={
                'extra': self.backend.get_extra()
            }
        )

    def _prepare_backup_folder(self):
        path = os.path.join(self.media_root, self.output_folder)
        return mkdir_p(path)

    def pre_backup_log(self, backup_setup, full_path, media_url):
        return BackupLog.objects.update_or_create(
            backup_setup=backup_setup,
            media_url=media_url,
            defaults={
                'file_path': full_path,
                'file_name': os.path.basename(full_path),
                'date_started': timezone.now()
            }
        )[0]

    def post_backup_log(self, backup_log, is_valid, message):
        now = timezone.now()
        file_size = (os.path.getsize(backup_log.file_path) if is_valid else 0)

        BackupLog.objects.filter(pk=backup_log.pk).update(
            date_modified=now,
            date_completed=now,
            date_validated=(now if is_valid else None),
            file_size=file_size,
            message=message
        )

    def backup_database(self, backup_setup):
        file_name = self.backend.get_backup_filename(backup_setup)
        full_path = os.path.join(self._prepare_backup_folder(), file_name)
        media_url = '%s%s' % (self.output_folder, file_name)

        backup_log = self.pre_backup_log(backup_setup, full_path, media_url)
        is_valid,  message = False, ''
        try:
            self.backend.do_backup(backup_setup, full_path)
            is_valid = True
        except Exception as err:
            message = str(err)

        self.post_backup_log(backup_log, is_valid, message)


import paramiko
from scp import SCPClient


class StorageUtils:

    def pre_sync_log(self, sync_setup, backup_log):
        return SyncLog.objects.update_or_create(
            sync_setup=sync_setup,
            backup_log=backup_log,
            defaults={
                'date_started': timezone.now()
            }
        )[0]

    def post_sync_log(self, sync_log, is_valid, message):
        now = timezone.now()

        SyncLog.objects.filter(pk=sync_log.pk).update(
            date_modified=now,
            date_completed=now,
            date_validated=(now if is_valid else None),
            message=message
        )
        BackupLog.objects.filter(pk=sync_log.backup_log.pk).update(date_synced=now)

    def create_ssh_client(self, server, port, user, password):
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(server, port, user, password)
        return client

    def get_sync_setup(self):
        return SyncSetup.objects.filter(is_active=True).first()

    def push_file_remote(self, backup_log):
        is_valid, message = False, ''

        sync_setup = self.get_sync_setup()
        if not is_file_exists(backup_log.file_path):
            is_valid = False
            message = 'Backup file does not exist on disk'
        elif not sync_setup:
            is_valid = False
            message = 'No sync setup record defined in db'
        else:
            sync_log = self.pre_sync_log(sync_setup, backup_log)
            try:
                self.do_sync(sync_setup, backup_log)
                is_valid = True
            except Exception as err:
                message = str(err)

            self.post_sync_log(sync_log, is_valid, message)

    # TODO: Move to better place
    def do_sync(self, sync_setup, backup_log):
        ssh = self.create_ssh_client(sync_setup.host, sync_setup.port, sync_setup.username, sync_setup.password)
        scp = SCPClient(ssh.get_transport())

        scp.put(backup_log.file_path, recursive=True, remote_path=sync_setup.remote_path)