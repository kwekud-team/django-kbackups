# Generated by Django 2.1.8 on 2019-10-29 01:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kbackups', '0003_auto_20191029_0111'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='syncsetup',
            name='unique_key',
        ),
    ]
