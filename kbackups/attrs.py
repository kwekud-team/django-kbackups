

class BackupSetupAttr:
    admin_list_display = ['unique_key', 'type', 'is_active']
    admin_list_filter = ['type', 'date_created']


class BackupLogAttr:
    admin_list_display = ['date_started', 'date_completed', 'date_validated', 'date_synced', 'file_name',
                          'get_file_size_mb', 'download_file']
    admin_list_filter = ['date_started', 'date_completed', 'date_synced', 'backup_setup']


class SyncSetupAttr:
    admin_list_display = ['type', 'host', 'port', 'username', 'is_active']
    admin_list_filter = ['type', 'date_created']


class SyncLogAttr:
    admin_list_display = ['date_started', 'date_completed', 'date_validated', 'sync_setup', 'backup_log']
    admin_list_filter = ['date_started', 'date_completed', 'sync_setup']
