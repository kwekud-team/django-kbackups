from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse

from kbackups.models import BackupSetup, BackupLog, SyncSetup, SyncLog
from kbackups.attrs import BackupSetupAttr, BackupLogAttr, SyncSetupAttr, SyncLogAttr


@admin.register(BackupSetup)
class BackupSetupAdmin(admin.ModelAdmin):
    list_display = BackupSetupAttr.admin_list_display
    list_filter = BackupSetupAttr.admin_list_filter
    search_fields = ['unique_key']
    change_list_template = "kbackups/admin/backup_setup_change_list.html"
    list_editable = ['is_active']

    actions = ['backup_entry']

    def backup_entry(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        url = reverse('kbackups:run_database')
        return HttpResponseRedirect("%s?pks=%s" % (url, ",".join(selected)))
    backup_entry.short_description = "Backup selected entries"


@admin.register(BackupLog)
class BackupLoginAdmin(admin.ModelAdmin):
    list_display = BackupLogAttr.admin_list_display
    list_filter = BackupLogAttr.admin_list_filter

    actions = ['sync_file']

    def sync_file(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        url = reverse('kbackups:sync_file')
        return HttpResponseRedirect("%s?pks=%s" % (url, ",".join(selected)))
    sync_file.short_description = "Sync selected log files"


@admin.register(SyncSetup)
class SyncSetupAdmin(admin.ModelAdmin):
    list_display = SyncSetupAttr.admin_list_display
    list_filter = SyncSetupAttr.admin_list_filter
    search_fields = ['unique_key']
    list_editable = ['is_active']


@admin.register(SyncLog)
class SyncLogAdmin(admin.ModelAdmin):
    list_display = SyncLogAttr.admin_list_display
    list_filter = SyncLogAttr.admin_list_filter
