from django.core.management.base import BaseCommand

from kbackups.utils import load_database_configs_from_settings


class Command(BaseCommand):
    help = 'Backups database'

    def handle(self, *args, **options):
        message = load_database_configs_from_settings()
        self.stdout.write(self.style.SUCCESS(message))
