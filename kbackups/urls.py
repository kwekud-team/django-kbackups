from django.urls import path
from kbackups import views


app_name = 'kbackups'

urlpatterns = [
    path('load-configs/<str:type>/', views.LoadConfigView.as_view(), name='load_configs'),
    path('run-backup/', views.RunDatabaseView.as_view(), name='run_database'),
    path('sync-file/', views.SyncFileView.as_view(), name='sync_file')
]
