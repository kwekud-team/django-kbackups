from django.db import models
from django.conf import settings
from django.utils.safestring import mark_safe
from jsonfield import JSONField

from kommons.utils.files import ByteConverter
from kbackups.constants import KbackupsK


class BackupSetup(models.Model):
    unique_key = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    extra = JSONField(default=dict)

    def __str__(self):
        return self.unique_key

    class Meta:
        unique_together = ['unique_key', 'type']


class BackupLog(models.Model):
    backup_setup = models.ForeignKey(BackupSetup, on_delete=models.CASCADE)
    file_name = models.CharField(max_length=50)
    file_path = models.CharField(max_length=250, null=True, blank=True)
    media_url = models.CharField(max_length=250, null=True, blank=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_started = models.DateTimeField(auto_now_add=True)
    date_completed = models.DateTimeField(null=True, blank=True)
    date_validated = models.DateTimeField(null=True, blank=True)
    date_synced = models.DateTimeField(null=True, blank=True)
    date_removed = models.DateTimeField(null=True, blank=True)
    message = models.TextField(null=True, blank=True)
    file_size = models.BigIntegerField(default=0)

    def __str__(self):
        return '%s - %s' % (self.date_started, self.file_name)

    class Meta:
        ordering = ('-date_started',)

    def get_file_size_mb(self):
        return ByteConverter().byte_2_mb(self.file_size)
    get_file_size_mb.short_description = 'File size (MB)'
    get_file_size_mb.admin_order_field = 'file_size'

    def download_file(self):
        if self.date_validated:
            url = '%s%s' % (settings.MEDIA_URL, self.media_url)
            html = '<a href="%s">Download</a>' % url
            return mark_safe(html)


SYNC_SETUP_TYPES = (
    (KbackupsK.SYNC_TYPE_SCP.value, 'SCP'),
    (KbackupsK.SYNC_TYPE_DROPBOX.value, 'Dropbox'),
)


class SyncSetup(models.Model):
    priority = models.PositiveSmallIntegerField(default=100)
    type = models.CharField(max_length=50, choices=SYNC_SETUP_TYPES)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    host = models.CharField(max_length=50)
    port = models.PositiveSmallIntegerField(default=0)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    remote_path = models.CharField(max_length=250)

    def __str__(self):
        return str(self.pk)

    class Meta:
        ordering = ('priority',)
        unique_together = ['host', 'type', 'username']


class SyncLog(models.Model):
    sync_setup = models.ForeignKey(SyncSetup, on_delete=models.CASCADE)
    backup_log = models.ForeignKey(BackupLog, on_delete=models.CASCADE)
    date_modified = models.DateTimeField(auto_now=True)
    date_started = models.DateTimeField(auto_now_add=True)
    date_completed = models.DateTimeField(null=True, blank=True)
    date_validated = models.DateTimeField(null=True, blank=True)
    message = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s' % self.date_started

    class Meta:
        ordering = ('-date_started',)
