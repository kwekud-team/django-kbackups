import setuptools

long_description = ''
try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except FileNotFoundError:
    pass

setuptools.setup(
     name='kbackups',
     version='0.1.2',
     # scripts=['kbackups'],
     author="KDanso",
     author_email="dev@kkdanso.com",
     description="Library for automating database and media backups",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://bitbucket.org/kwekud-team/kbackups",
     packages=setuptools.find_packages(),
     include_package_data=True,
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
     install_requires=[
        'django-dbbackup>=3.2.0',
        'jsonfield2>=3.0.3',
        'paramiko>=2.6.0',
        'scp>=0.13.2'
     ]
)
