from webapp.codes.conf.celery_config import app

from kbackups.models import BackupSetup, BackupLog
from kbackups.utils import BackupUtils, StorageUtils


@app.task(bind=True, name='kbackups.tasks.bgr_backup_entries')
def bgr_backup_entries(self, backup_setup_type=None):
    filters = {}
    if backup_setup_type:
        filters = {"type": backup_setup_type}

    for backup_setup in BackupSetup.objects.filter(is_active=True, **filters):
        BackupUtils(backup_setup.type).backup_database(backup_setup)
    return ''


@app.task(bind=True, name='kbackups.tasks.bgr_sync_log_files')
def bgr_sync_log_files(self):
    for backup_log in BackupLog.objects.filter(date_synced__isnull=True):
        StorageUtils().push_file_remote(backup_log)
    return ''
